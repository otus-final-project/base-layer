terraform {
  backend "gcs" {
    bucket = "tf-state-staging2020"
    prefix = "terraform/state"
  }
}
