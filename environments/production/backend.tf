terraform {
  backend "gcs" {
    bucket = "tf-state-production2020"
    prefix = "terraform/state"
  }
}
