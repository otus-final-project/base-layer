module production_environment {
  source                               = "../../modules/environment"
  project                              = var.project
  zone                                 = var.zone
  name                                 = "production"
  worker_machine_type                  = "e2-highmem-2"
  worker_disk_size                     = "20"
  preemptible                           = false
  min_node_count                       = 1
  max_node_count                       = 3
}
