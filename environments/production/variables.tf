variable project {
  description = "GCP project ID"
  type        = string
}

variable zone {
  description = "GCP zone"
  type        = string
  default     = "us-central1-a"
}

variable region {
  description = "GCP region"
  type        = string
  default     = "us-central1"
}

