variable name {
  description = "Environment name"
  type        = string
}

variable "region" {
  type        = string
  default     = "us-central1"
  description = "Region name"
}

variable zone {
  description = "Zone name"
  default     = "us-central1-a"
  type        = string
}

variable worker_machine_type {
  description = "Worker node's machine type"
  type        = string
  default     = "e2-standard-2"
}

variable worker_disk_size {
  description = "Boot disk size (GB) for worker nodes"
  type        = number
  default     = 20
}

variable min_node_count {
  description = "Minimum nodes in zone"
  type        = number
  default     = 1
}

variable max_node_count {
  description = "Maximum nodes in zone"
  type        = number
  default     = 2
}

variable "istio_disabled" {
  type        = bool
  default     = true
  description = "Istio disabled"
}

variable "project" {
  type        = string
  description = "Project id"
}

variable "http_load_balancing_disabled" {
  type        = bool
  default     = true
  description = "HTTP L7 load balancing disabled"
}

variable "preemptible" {
  type        = bool
  default     = false
  description = "Preemtible instance?"
}
