resource google_container_cluster "cluster" {
  provider                 = google-beta
  name                     = var.name
  location                 = var.region
  remove_default_node_pool = true
  initial_node_count       = 1
  monitoring_service       = "none"
  logging_service          = "none"
  min_master_version       = "1.15"

  network_policy {
    enabled = false
  }

  // temporarily disable istio
  addons_config {
    istio_config {
      disabled = var.istio_disabled
    }
    http_load_balancing {
      disabled = var.http_load_balancing_disabled
    }
  }
  maintenance_policy {
    daily_maintenance_window {
      start_time = "02:00"
    }
  }
}

resource google_container_node_pool "node_pool-1a" {
  name               = "pr-1a"
  location           = var.region
  node_locations      = ["us-central1-a"]
  cluster            = google_container_cluster.cluster.name
  version            = "1.15.11-gke.5"
  initial_node_count = 1

  autoscaling {
    min_node_count = var.min_node_count
    max_node_count = var.max_node_count
  }

  node_config {
    preemptible  = var.preemptible
    machine_type = var.worker_machine_type
    disk_size_gb = var.worker_disk_size

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/monitoring.write",
    ]
  }

  timeouts {
    create = "60m"
    update = "60m"
    delete = "60m"
  }
}

resource google_container_node_pool "node_pool-1b" {
  name               = "pr-1b"
  location           = var.region
  node_locations      = ["us-central1-b"]
  cluster            = google_container_cluster.cluster.name
  version            = "1.15.11-gke.5"
  initial_node_count = 1

  autoscaling {
    min_node_count = var.min_node_count
    max_node_count = var.max_node_count
  }

  node_config {
    preemptible  = true
    machine_type = var.worker_machine_type
    disk_size_gb = var.worker_disk_size

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/monitoring.write",
    ]
  }

  timeouts {
    create = "60m"
    update = "60m"
    delete = "60m"
  }
}

resource google_compute_address "address" {
  name         = var.name
  project      = var.project
  address_type = "EXTERNAL"
}
