## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| google | n/a |
| google-beta | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| http\_load\_balancing\_disabled | HTTP L7 load balancing disabled | `bool` | `true` | no |
| istio\_disabled | Istio disabled | `bool` | `true` | no |
| max\_node\_count | Maximum nodes in zone | `number` | `2` | no |
| min\_node\_count | Minimum nodes in zone | `number` | `1` | no |
| name | Environment name | `string` | n/a | yes |
| preemtible | Preemtible instance? | `bool` | `false` | no |
| project | Project id | `string` | n/a | yes |
| region | Region name | `string` | `"us-central1"` | no |
| worker\_disk\_size | Boot disk size (GB) for worker nodes | `number` | `20` | no |
| worker\_machine\_type | Worker node's machine type | `string` | `"e2-standard-2"` | no |
| zone | Zone name | `string` | `"us-central1-a"` | no |

## Outputs

| Name | Description |
|------|-------------|
| cluster\_endpoint | Public endpoint of Kubernetes cluster |
| service\_external\_ip\_address | External IP address for ingress |

