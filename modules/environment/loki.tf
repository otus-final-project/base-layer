resource "google_storage_bucket" "loki-bucket" {
  name          = "loki-${var.project}"
  location      = "US"
  force_destroy = false

  bucket_policy_only = true
}
