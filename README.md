# base-layer

This repository supports deployment to 2 different environments: staging and production

## repository structure

```
.
├── environments
│   ├── production
│   │   ├── backend.tf
│   │   ├── main.tf
│   │   ├── provider.tf
│   │   └── variables.tf
│   └── staging
│       ├── backend.tf
│       ├── main.tf
│       ├── provider.tf
│       └── variables.tf
├── modules
│   └── environment
│       ├── gke.tf
│       ├── loki.tf
│       ├── output.tf
│       ├── service-accounts.tf
│       └── variables.tf
└── README.md
```
The cluster is deployed via Terraform and Gitlab CI:
 * the cluster is being created at GKE;
 * the service account for Gitlab is being created;
 * the bucket for Terraform state is being created;
 * the bucket for Loki is being created.

The CI/CD pipeline start via commit and contain five stages:
  * validate (auto start);
  * plan (auto start);
  * apply (manual start);
  * verify (auto start);
  * destroy (manual start).
